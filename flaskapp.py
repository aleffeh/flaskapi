# encoding: utf-8
from flask import Flask, jsonify, request
import json
from selenium import webdriver
from bs4 import BeautifulSoup as soup
import datetime
import os
import threading
from selenium.webdriver.chrome.options import Options

class FacebookChecker:
 
    def __init__(self):
        self.last_check = ""
        self.rl_status = ""

    def run(self):
        if self.CheckDate():
            t = threading.Thread(self.GetRls())
            t.start()
            return jsonify({"Last Check" : self.last_check, "Status" : self.rl_status})
        else:
            file = open('status.rlst','r')
            lines = file.readlines()
            file.close()
            return jsonify({"Last Check" : lines[0].strip(), "Status" : lines[1].strip()})


    def GetRls(self):
        email = 'felipexk1@terra.com.br'
        password = '1ac1ye123,'
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--single-process')
        options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(options=options)
        driver.get("https://www.facebook.com")

        driver.find_element_by_name('email').send_keys(email)
        driver.find_element_by_name('pass').send_keys(password)
        driver.find_element_by_id('loginbutton').click()

        driver.get("https://www.facebook.com/tainan.rathlef")
        relationship = driver.page_source

        sp = soup(relationship,features="html.parser")
        rl = sp.find("div", {"id" : "intro_container_id" })
        ux = []
        for i in rl.div.div.ul:
            ux.append(i)

        rls = ux[5].get_text()

        driver.close()
        self.last_check = datetime.datetime.now().date().strftime('%d/%m/%Y')
        self.rl_status = rls

        self.Persist(rls)

    def Persist(self,rls):
        file = open('status.rlst','w')
        lc = datetime.datetime.now().date().strftime('%d/%m/%Y')
        string = "{} \n{}".format(lc,rls)
        file.write(string)

    def CheckDate(self):
        if os.path.isfile('status.rlst'):
            file = open('status.rlst','r')
            lc = str(datetime.datetime.now().date().strftime('%d/%m/%Y'))
            dt = file.readlines(1)[0].strip()
            if lc == dt :
                file.close()
                return False
            else :
                file.close()
                return True 
        else: return True


app = Flask(__name__)

@app.route("/")
def hello():
    return jsonify({"version": 1})

@app.route('/status')
def status():
    return FacebookChecker().run()
    

if __name__ == '__main__':
  app.run()
